package it.unibs.ing.se.greetings;

import java.time.LocalDate;

public class Employee {
    private final String firstName;
    private final String lastName;
    private final String email;
    private final LocalDate birthday;

    public Employee(String firstName, String lastName, String email, LocalDate birthday) {
        this.firstName = firstName;

        this.lastName = lastName;
        this.email = email;
        this.birthday = birthday;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getEmail() {
        return email;
    }

    public LocalDate getBirthday() {
        return birthday;
    }
}
