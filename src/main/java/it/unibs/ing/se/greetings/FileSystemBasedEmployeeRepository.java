package it.unibs.ing.se.greetings;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class FileSystemBasedEmployeeRepository {
    private final Path dataFile;

    public FileSystemBasedEmployeeRepository(Path dataFile) {
        this.dataFile = dataFile;
    }

    public List<Employee> ReadAllEmployees() throws IOException {
        var rows = Files.readAllLines(dataFile);
        return rows
                .stream()
                .skip(1)
                .map(FileSystemBasedEmployeeRepository::ReadEmployee)
                .collect(Collectors.toList());
    }

    private static Employee ReadEmployee(String row) {
        var fields = Arrays.asList(row.split(","))
                .stream()
                .map(String::trim)
                .collect(Collectors.toList());

        return new Employee(fields.get(1), fields.get(0), fields.get(3), LocalDate.parse(fields.get(2)));
    }
}
