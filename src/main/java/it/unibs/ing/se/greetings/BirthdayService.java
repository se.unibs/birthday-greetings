package it.unibs.ing.se.greetings;

import java.time.LocalDate;

public class BirthdayService {
    private final EmployeeRepository repo;
    private final GreetingsNotifier notifier;

    public BirthdayService(EmployeeRepository repo, GreetingsNotifier notifier) {
        this.repo = repo;
        this.notifier = notifier;
    }

    public void sendGreetings(LocalDate today) {
        var employees = repo.getAllEmployees();
        for (var employee : employees) {
            if(employee.getBirthday().getDayOfMonth() == today.getDayOfMonth() && employee.getBirthday().getMonth() == today.getMonth()) {
                notifier.notifyBirthdayGreetings(employee);
            }
        }
    }
}
