package it.unibs.ing.se.greetings;

public interface GreetingsNotifier {
    void notifyBirthdayGreetings(Employee employee);
}
