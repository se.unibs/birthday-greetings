package it.unibs.ing.se.greetings;

import java.util.List;

public interface EmployeeRepository {
    List<Employee> getAllEmployees();
}
