package it.unibs.ing.se.greetings;

import org.junit.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class BirthdayServiceTest {
    @Test
    public void noBirthdayToday() {
        EmployeeRepository repo = new InMemoryEmployeeRepository();
        GreetingsNotifier notifier = new SpyNotifier();
        var service = new BirthdayService(repo, notifier);
        var today = LocalDate.of(2020, 5, 16);
        service.sendGreetings(today);
        assertThat(((SpyNotifier)notifier).getSentGreetingsCount(), is(equalTo(0)));
    }

    @Test
    public void oneBirthdayToday() {
        EmployeeRepository repo = new InMemoryEmployeeRepository(
                new Employee("Pietro", "Martinelli", "pietrom@example.com", LocalDate.of(1978, 3, 19)),
                new Employee("Paolino", "Paperino", "paperino@example.com", LocalDate.of(1930, 1, 1))
        );
        GreetingsNotifier notifier = new SpyNotifier();
        var service = new BirthdayService(repo, notifier);
        var today = LocalDate.of(2021, 3, 19);
        service.sendGreetings(today);
        assertThat(((SpyNotifier)notifier).getSentGreetingsCount(), is(equalTo(1)));
    }

    @Test
    public void manyBirthdaysToday() {
        EmployeeRepository repo = new InMemoryEmployeeRepository(
                new Employee("Pietro", "Martinelli", "pietrom@example.com", LocalDate.of(1978, 3, 19)),
                new Employee("Paolino", "Paperino", "paperino@example.com", LocalDate.of(1930, 1, 1)),
                new Employee("Mickey", "Mouse", "mmouse@example.com", LocalDate.of(1920, 1, 1))
        );
        GreetingsNotifier notifier = new SpyNotifier();
        var service = new BirthdayService(repo, notifier);
        var today = LocalDate.of(2021, 1, 1);
        service.sendGreetings(today);
        var notifiedEmployeeNames = ((SpyNotifier)notifier).getNotifiedEmployees().stream().map(Employee::getFirstName).collect(Collectors.toList());
        assertThat("Paolino", notifiedEmployeeNames.contains("Paolino"));
        assertThat("Mickey", notifiedEmployeeNames.contains("Mickey"));
    }
}

class SpyNotifier implements GreetingsNotifier {
    private final List<Employee> notifiedEmployees = new ArrayList<>();

    public int getSentGreetingsCount() {
        return notifiedEmployees.size();
    }

    @Override
    public void notifyBirthdayGreetings(Employee employee) {
        notifiedEmployees.add(employee);
    }

    public List<Employee> getNotifiedEmployees() {
        return notifiedEmployees;
    }
}

class InMemoryEmployeeRepository implements EmployeeRepository {
    private final List<Employee> employees;

    public InMemoryEmployeeRepository(Employee ... employees) {
        this.employees = Arrays.asList(employees);
    }

    @Override
    public List<Employee> getAllEmployees() {
        return employees;
    }
}
