package it.unibs.ing.se.greetings;

import org.junit.Test;

import java.nio.file.Files;
import java.time.LocalDate;
import java.util.Arrays;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class FileSystemBasedEmployeeRepositoryTest {
    @Test
    public void shouldReadAnEmptyEmployeeListWhenInputFileContainsOnlyHeaderRow() throws Exception {
        var tempFile = Files.createTempFile("employee", ".txt");
        Files.write(tempFile, Arrays.asList("last_name, first_name, date_of_birth, email"));
        var repo = new FileSystemBasedEmployeeRepository(tempFile);
        var employees = repo.ReadAllEmployees();
        assertThat(employees.size(), is(equalTo(0)));
    }

    @Test
    public void shouldReadAnEmployeeForEachDataFileRow() throws Exception {
        var tempFile = Files.createTempFile("employee", ".txt");
        Files.write(tempFile, Arrays.asList(
                "last_name, first_name, date_of_birth, email",
                "Ann, Mary, 1975-09-11, mary.ann@foobar.com",
                "Martinelli, Pietro, 1978-03-19, pietrom@foobar.com",
                "Kovic, Ron, 1946-07-04, ron.kovic@foobar.com"
        ));
        var repo = new FileSystemBasedEmployeeRepository(tempFile);
        var employees = repo.ReadAllEmployees();
        assertThat(employees.size(), is(equalTo(3)));
    }

    @Test
    public void shouldReadEmployeeLastNameData() throws Exception {
        var tempFile = Files.createTempFile("employee", ".txt");
        Files.write(tempFile, Arrays.asList(
                "last_name, first_name, date_of_birth, email",
                "Martinelli, Pietro, 1978-03-19, pietrom@foobar.com"
        ));
        var repo = new FileSystemBasedEmployeeRepository(tempFile);
        var first = repo.ReadAllEmployees().get(0);
        assertThat(first.getLastName(), is(equalTo("Martinelli")));
    }

    @Test
    public void shouldReadEmployeeFirstNameData() throws Exception {
        var tempFile = Files.createTempFile("employee", ".txt");
        Files.write(tempFile, Arrays.asList(
                "last_name, first_name, date_of_birth, email",
                "Martinelli, Pietro, 1978-03-19, pietrom@foobar.com"
        ));
        var repo = new FileSystemBasedEmployeeRepository(tempFile);
        var first = repo.ReadAllEmployees().get(0);
        assertThat(first.getFirstName(), is(equalTo("Pietro")));
    }

    @Test
    public void shouldReadEmployeeEmailAddressData() throws Exception {
        var tempFile = Files.createTempFile("employee", ".txt");
        Files.write(tempFile, Arrays.asList(
                "last_name, first_name, date_of_birth, email",
                "Martinelli, Pietro, 1978-03-19, pietrom@foobar.com"
        ));
        var repo = new FileSystemBasedEmployeeRepository(tempFile);
        var first = repo.ReadAllEmployees().get(0);
        assertThat(first.getEmail(), is(equalTo("pietrom@foobar.com")));
    }

    @Test
    public void shouldReadEmployeeBirthdayAddressData() throws Exception {
        var tempFile = Files.createTempFile("employee", ".txt");
        Files.write(tempFile, Arrays.asList(
                "last_name, first_name, date_of_birth, email",
                "Martinelli, Pietro, 1978-03-19, pietrom@foobar.com"
        ));
        var repo = new FileSystemBasedEmployeeRepository(tempFile);
        var first = repo.ReadAllEmployees().get(0);
        assertThat(first.getBirthday(), is(equalTo(LocalDate.of(1978, 3, 19))));
    }
}
